import subprocess

def is_ipfs_initialized():
	ipfs_cat=subprocess.Popen(['ipfs', 'cat', '/ipfs/QmQPeNsJPyVWPFDVHb77w8G42Fvo15z4bG2X8D2GhfbSXc/readme'],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
	ipfs_cat.stdout.read()
	#ipfs_cat.stdout.close()
	ipfs_cat.wait()
	if(ipfs_cat.returncode)==1:
		return False
	else:
		return True
def initialize_ipfs():
	ipfs_init=subprocess.Popen(['ipfs','init'])
	ipfs_init.wait()

if not is_ipfs_initialized(): 
	initialize_ipfs()



